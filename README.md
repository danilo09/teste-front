# Frontend Test

O objetivo do desafio é simples: Montar e estruturar uma pagina utilizando HTML, CSS e JavaScript seguindo o layout fornecido.

## Design

- O Design apresentado deve seguir o arquivo da pasta Layout/front-layout.xd
- Baseado neste layout, faça uma adaptação responsiva para celulares.

## Como realizar o teste

- Crie um Readme com uma descrição de como rodar seu projeto.
- Descreva as funcionalidades do seu desafio, venda seu peixe! Conte-nos o que utilizou, qual foi a metodologia escolhida. Por exemplo, usei o plugin "x" para slider, ou utilizei grunt ou gulp para compilar o css.
- Em caso de dúvidas, entre em contato com yago@shoppub.com.br

## Dicas
 
- Usamos Mobile first, então recomendamos começar pelo mobile e depois ir para o desktop.
- Use bootstrap 4 para facilitar a criação das colunas.
- Amamos CSS responsivo, organizado, modular e feito com pré-processadores. Utilizamos Grunt e Gulp aqui na empresa. (opcional)
- Nosso desenvolvimento é feito com Pixel Perfect, então se atente aos detalhes como espaçamentos, tamanhos e estilos de fonte.

## Critérios de avaliação

- Alcançar os objetivos propostos.
- Qualidade de código.
- Fidelidade ao design proposto.
- Adaptação mobile.

## Após finalizar o teste

- Enviar para o e-mail yago@shoppub.com.br com o assunto #Teste front-end [nome do desenvolvedor].

